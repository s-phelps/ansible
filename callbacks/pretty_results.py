
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

try:
    import simplejson as json
except ImportError:
    import json

# Fields to reformat output for
FIELDS = [
    'cmd',
    'command',
    'start',
    'end',
    'delta',
    'msg',
    'stdout',
    'stderr',
    'results'
]


class CallbackModule(object):

    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'notification'
    CALLBACK_NAME = 'pretty_results'
    CALLBACK_NEEDS_WHITELIST = False

    def pretty_print(self, data):
        if type(data) == dict:
            for field in FIELDS:
                no_log = data.get('_ansible_no_log')
                if field in data.keys() and data[field] and not no_log:
                    output = self._format_output(data[field])
                    print("{0}: {1}".format(field, output.replace("\\n","\n")))

    def _format_output(self, output):
        # Strip unicode
        if type(output) == unicode:
            output = output.encode('ascii', 'replace')

        # If output is a dict
        if type(output) == dict:
            return json.dumps(output, indent=2)

        # If output is a list of dicts
        if type(output) == list and type(output[0]) == dict:
            # This gets a little complicated because it potentially means
            # nested results, usually because of with_items.
            real_output = list()
            for index, item in enumerate(output):
                copy = item
                if type(item) == dict:
                    for field in FIELDS:
                        if field in item.keys():
                            copy[field] = self._format_output(item[field])
                real_output.append(copy)
            return json.dumps(output, indent=2)

        # If output is a list of strings
        if type(output) == list and type(output[0]) != dict:
            # Strip newline characters
            real_output = list()
            for item in output:
                if "\n" in item:
                    for string in item.split("\n"):
                        real_output.append(string)
                else:
                    real_output.append(item)

            # Reformat lists with line breaks only if the total length is
            # >75 chars
            if len("".join(real_output)) > 75:
                return "\n" + "\n".join(real_output)
            else:
                return " ".join(real_output)

        # Otherwise it's a string, (or an int, float, etc.) just return it
        return str(output)

    def runner_on_failed(self, host, res, ignore_errors=False):
        self.pretty_print(res)

    def runner_on_ok(self, host, res):
        self.pretty_print(res)

    def runner_on_unreachable(self, host, res):
        self.pretty_print(res)

    def runner_on_async_poll(self, host, res, jid, clock):
        self.pretty_print(res)

    def runner_on_async_ok(self, host, res, jid):
        self.pretty_print(res)

    def runner_on_async_failed(self, host, res, jid):
        self.pretty_print(res)

    def v2_runner_on_failed(self, result, ignore_errors=False):
        self.pretty_print(result._result)

    def v2_runner_on_ok(self, result):
        self.pretty_print(result._result)

    def v2_runner_on_unreachable(self, result):
        self.pretty_print(result._result)

    def v2_runner_on_async_poll(self, result):
        self.pretty_print(result._result)

    def v2_runner_on_async_ok(self, host, result):
        self.pretty_print(result._result)

    def v2_runner_on_async_failed(self, result):
        self.pretty_print(result._result)
